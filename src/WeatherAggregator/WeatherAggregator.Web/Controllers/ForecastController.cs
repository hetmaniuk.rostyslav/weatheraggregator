using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WeatherAggregator.Data.Infrastructure.Weather.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels.General;

namespace WeatherAggregator.Web.Controllers
{
    [Route("api/forecasts")]
    [ApiController]
    public class ForecastController : Controller
    {
        private readonly IWeatherProviderService _weatherProviderService;
        
        public ForecastController(IWeatherProviderService weatherProviderService)
        {
            _weatherProviderService = weatherProviderService;
        }

        [HttpGet("daily")]
        public async Task<IActionResult> GetDailyForecast([FromQuery] ProviderType providerType)
        {
            var weatherProvider = GetWeatherProvider(providerType);
            var result = await weatherProvider.GetDailyForecastAsync();
            return Json(new Response(result));
        }

        [HttpGet("weekly")]
        public async Task<IActionResult> GetWeeklyForecasts([FromQuery] ProviderType providerType)
        {
            var weatherProvider = GetWeatherProvider(providerType);
            var result = await weatherProvider.GetWeeklyForecastsAsync();
            return Json(new Response(result));
        }

        private IWeatherProvider GetWeatherProvider(ProviderType type)
            => _weatherProviderService.GetProviderAdapter(type);
        
    }
}
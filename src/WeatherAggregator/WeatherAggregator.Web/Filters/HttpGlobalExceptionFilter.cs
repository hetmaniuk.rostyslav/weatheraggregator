using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;
using WeatherAggregator.Data.Infrastructure.Weather.Exceptions;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels.General;

namespace WeatherAggregator.Web.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var error = ExceptionToError(context.Exception);

            var response = new Response(error);

            context.ExceptionHandled = true;

            context.Result = new ObjectResult(response)
            {
                StatusCode = (int)error.StatusCode
            };
        }

        private Error ExceptionToError(Exception exception)
        {
            if (exception is WeatherDomainException domainException)
            {
                return new Error(domainException.ErrorCode, domainException.StatusCode, domainException.Message);
            }
            else
            {
                return new Error(WeatherErrorCode.Unknown, HttpStatusCode.InternalServerError);
            }
        }
    }
}
using System;
using System.Net;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;

namespace WeatherAggregator.Data.Infrastructure.Weather.Exceptions
{
    public class WeatherDomainException : Exception
    {
        public WeatherErrorCode ErrorCode { get; }
        public HttpStatusCode StatusCode { get; }
        
        public WeatherDomainException()
        { }

        public WeatherDomainException(WeatherErrorCode errorCode, HttpStatusCode statusCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }

        public WeatherDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
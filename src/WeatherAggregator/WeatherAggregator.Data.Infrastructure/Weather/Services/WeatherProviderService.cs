using System.Net;
using WeatherAggregator.Data.Infrastructure.Weather.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;
using WeatherAggregator.Data.Infrastructure.Weather.Exceptions;

namespace WeatherAggregator.Data.Infrastructure.Weather.Services
{
    public class WeatherProviderService : IWeatherProviderService
    {
        public IWeatherProvider GetProviderAdapter(ProviderType providerType)
        {
            switch (providerType)
            {
                case ProviderType.Owm: 
                    return new OpenWeatherMapProviderAdapter();
                case ProviderType.Aw: 
                    return new AccuWeatherProviderAdapter();
                default: 
                    throw new WeatherDomainException(
                        WeatherErrorCode.InvalidProvider,
                        HttpStatusCode.BadRequest, 
                        "This provider does not support provider");
            }
        }
    }
}
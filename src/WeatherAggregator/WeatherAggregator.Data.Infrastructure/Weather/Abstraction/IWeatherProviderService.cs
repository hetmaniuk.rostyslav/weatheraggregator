using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;

namespace WeatherAggregator.Data.Infrastructure.Weather.Abstraction
{
    public interface IWeatherProviderService
    {
        IWeatherProvider GetProviderAdapter(ProviderType providerType);
    }
}
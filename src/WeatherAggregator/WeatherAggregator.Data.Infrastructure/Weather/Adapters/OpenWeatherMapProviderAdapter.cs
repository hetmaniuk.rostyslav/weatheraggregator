using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestEase;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Mappers;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.REST;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters
{
    public class OpenWeatherMapProviderAdapter : IWeatherProvider
    {
        private readonly IOpenWeatherMapApiClient _apiClient;
        private const string Units = "metric";

        public OpenWeatherMapProviderAdapter()
        {
            _apiClient = RestClient.For<IOpenWeatherMapApiClient>(Constants.OpenWeatherMapServiceDetail.BasePath);
        }
        
        public async Task<ForecastResponseModel> GetDailyForecastAsync()
        {
            var result = await _apiClient.GetDailyForecast(Constants.DefaultLocation.Latitude,
                Constants.DefaultLocation.Longitude,
                Units,
                Constants.OpenWeatherMapServiceDetail.ApiKey);
            return result.ToResponseModel();
        }

        public async Task<IEnumerable<ForecastResponseModel>> GetWeeklyForecastsAsync()
        {
            var result = await _apiClient.GetWeaklyForecast(Constants.DefaultLocation.Latitude,
                Constants.DefaultLocation.Longitude,
                Units,
                Constants.OpenWeatherMapServiceDetail.ApiKey);
            var forecast = result.List.GroupBy(x => x.DtTxt.Value.Day).Select(f => f.FirstOrDefault()).ToList();
            result.List = forecast;
            return result.ToResponseModel();
        }
    }
}
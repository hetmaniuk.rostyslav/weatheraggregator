using System;
using System.Collections.Generic;
using System.Linq;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Mappers
{
    public static class OpenWeatherMapMapper
    {
        public static ForecastResponseModel ToResponseModel(this DailyOpenWeatherMapModel model)
        {
            var item = new ForecastResponseModel();
            item.Date = model.DtTxt ?? DateTimeOffset.UtcNow.Date;
            item.MinTemp = model.Main.TempMin;                
            item.MaxTemp = model.Main.TempMax;
            
            item.Temp = model.Main.Temp;
                
            item.WindDirection = model.Wind.Deg;                
            item.WindSpeed = model.Wind.Speed;
            return item;
        }
        
        public static IEnumerable<ForecastResponseModel> ToResponseModel(this WeeklyOpenWeatherMapModel model)
        {
            var result = model.List.Select(w => w.ToResponseModel()).ToList();
            return result;
        }
    }
}
using System.Collections.Generic;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Mappers
{
    public static class AccuWeatherMapper
    {
        public static IEnumerable<ForecastResponseModel> ToResponseModel(this AccuWeatherModel model)
        {
            var result = new List<ForecastResponseModel>();
            foreach (var forecast in model.DailyForecasts)
            {            
                var item = new ForecastResponseModel();
                item.Date = forecast.Date;
                item.MinTemp = forecast.Temperature.Minimum.Value;                
                item.MaxTemp = forecast.Temperature.Maximum.Value;
                
                item.WindDirection = forecast.Day.Wind.Direction.Degrees;                
                item.WindSpeed = forecast.Day.Wind.Speed.Value;
                result.Add(item);
            }
            return result;
        }
    }
}
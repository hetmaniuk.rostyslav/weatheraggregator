using System.Threading.Tasks;
using RestEase;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.REST
{
    [AllowAnyStatusCode]
    public interface IAccuWeatherApiClient
    {
        [Get("forecasts/v1/daily/1day/{code}")]
        Task<AccuWeatherModel> GetDailyForecast(
            [Path] string code, 
            [Query] string apikey, 
            [Query] string language = "en-US",
            [Query] bool details = true,
            [Query] bool metric = true);
        
        [Get("forecasts/v1/daily/5day/{code}")]
        Task<AccuWeatherModel> GetWeaklyForecast(
            [Path] string code, 
            [Query] string apikey, 
            [Query] string language = "en-US",
            [Query] bool details = true,
            [Query] bool metric = true);
    }

    public interface IOpenWeatherMapApiClient
    {
        [Get("data/2.5/weather")]
        Task<DailyOpenWeatherMapModel> GetDailyForecast(
            [Query] double lat, 
            [Query] double lon, 
            [Query] string units,
            [Query] string appid);
        
        [Get("data/2.5/forecast")]
        Task<WeeklyOpenWeatherMapModel> GetWeaklyForecast(
            [Query] double lat, 
            [Query] double lon, 
            [Query] string units,
            [Query] string appid);
    }
}
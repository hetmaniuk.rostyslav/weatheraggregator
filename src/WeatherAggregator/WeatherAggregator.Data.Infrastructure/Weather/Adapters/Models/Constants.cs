using System.Collections.Generic;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models
{
    public class Constants
    {
        public static readonly IEnumerable<ServiceDetail> Services = new List<ServiceDetail>()
        {
            OpenWeatherMapServiceDetail,AccuWeatherServiceDetail
        };

        public static readonly ServiceDetail OpenWeatherMapServiceDetail = new ServiceDetail()
        {
            Name = "Open Weather Map",
            ApiKey = "c463c70a15ca641ad134b5b6a0651a84",
            BasePath = "http://api.openweathermap.org"
        };

        public static readonly ServiceDetail AccuWeatherServiceDetail = new ServiceDetail()
        {
            Name = "AccuWeather",
            ApiKey = "YUzw8pvfgeAZGTvAneYcHeHY5o9nMFt8",
            BasePath = "http://dataservice.accuweather.com"
        };

        public static readonly Location DefaultLocation = new Location()
        {
            Latitude = 49.55,
            Longitude = 25.59,
            Code = "325936"
        };
    }
}
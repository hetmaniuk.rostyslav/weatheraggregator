namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models
{
    public class ServiceDetail
    {
        public string Name { get; set; } 
        public string ApiKey { get; set; }
        public string BasePath { get; set; }
    }
}
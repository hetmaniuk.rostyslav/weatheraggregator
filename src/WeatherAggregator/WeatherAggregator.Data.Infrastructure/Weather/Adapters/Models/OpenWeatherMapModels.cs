using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models
{
    public class WeeklyOpenWeatherMapModel
    {
        public List<DailyOpenWeatherMapModel> List { get; set; }
    }

    public class DailyOpenWeatherMapModel
    {
        public long Dt { get; set; }
        public Main Main { get; set; }
        public List<Weather> Weather { get; set; }
        public Clouds Clouds { get; set; }
        public Wind Wind { get; set; }
        public Rain Rain { get; set; }
        public Sys Sys { get; set; }
        [JsonProperty("dt_txt")]
        public DateTimeOffset? DtTxt { get; set; }
    }

    public class Clouds
    {
        public long All { get; set; }
    }

    public class Main
    {
        public double Temp { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double Pressure { get; set; }
        public double SeaLevel { get; set; }
        public double GrndLevel { get; set; }
        public long Humidity { get; set; }
        public long TempKf { get; set; }
    }

    public class Rain
    {
        public double? The3H { get; set; }
    }

    public class Sys
    {
        public string Pod { get; set; }
    }

    public class Weather
    {
        public long Id { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }

    public class Wind
    {
        public double Speed { get; set; }
        public double Deg { get; set; }
    }

}
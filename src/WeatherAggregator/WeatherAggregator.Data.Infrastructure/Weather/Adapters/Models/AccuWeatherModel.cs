using System;
using System.Collections.Generic;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models
{
    public class AccuWeatherModel
    {
        public Headline Headline { get; set; }
        public List<DailyForecast> DailyForecasts { get; set; }
    }

    public class DailyForecast
    {
        public DateTimeOffset Date { get; set; }
        public long EpochDate { get; set; }
        public Sun Sun { get; set; }
        public Moon Moon { get; set; }
        public RealFeelTemperature Temperature { get; set; }
        public RealFeelTemperature RealFeelTemperature { get; set; }
        public RealFeelTemperature RealFeelTemperatureShade { get; set; }
        public long HoursOfSun { get; set; }
        public DegreeDaySummary DegreeDaySummary { get; set; }
        public List<AirAndPollen> AirAndPollen { get; set; }
        public Day Day { get; set; }
        public Day Night { get; set; }
        public List<string> Sources { get; set; }
        public Uri MobileLink { get; set; }
        public Uri Link { get; set; }
    }

    public class AirAndPollen
    {
        public string Name { get; set; }
        public long Value { get; set; }
        public string Category { get; set; }
        public long CategoryValue { get; set; }
        public string Type { get; set; }
    }

    public class Day
    {
        public long Icon { get; set; }
        public string IconPhrase { get; set; }
        public bool HasPrecipitation { get; set; }
        public string PrecipitationType { get; set; }
        public string PrecipitationIntensity { get; set; }
        public string ShortPhrase { get; set; }
        public string LongPhrase { get; set; }
        public long PrecipitationProbability { get; set; }
        public long ThunderstormProbability { get; set; }
        public long RainProbability { get; set; }
        public long SnowProbability { get; set; }
        public long IceProbability { get; set; }
        public AccuWind Wind { get; set; }
        public AccuWind WindGust { get; set; }
        public Ice TotalLiquid { get; set; }
        public Ice Rain { get; set; }
        public Ice Snow { get; set; }
        public Ice Ice { get; set; }
        public long HoursOfPrecipitation { get; set; }
        public long HoursOfRain { get; set; }
        public long HoursOfSnow { get; set; }
        public long HoursOfIce { get; set; }
        public long CloudCover { get; set; }
    }

    public class Ice
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public long UnitType { get; set; }
    }

    public class AccuWind
    {
        public Ice Speed { get; set; }
        public Direction Direction { get; set; }
    }

    public class Direction
    {
        public long Degrees { get; set; }
        public string Localized { get; set; }
        public string English { get; set; }
    }

    public class DegreeDaySummary
    {
        public Ice Heating { get; set; }
        public Ice Cooling { get; set; }
    }

    public class Moon
    {
        public DateTimeOffset Rise { get; set; }
        public long EpochRise { get; set; }
        public DateTimeOffset Set { get; set; }
        public long EpochSet { get; set; }
        public string Phase { get; set; }
        public long Age { get; set; }
    }

    public class RealFeelTemperature
    {
        public Ice Minimum { get; set; }
        public Ice Maximum { get; set; }
    }

    public class Sun
    {
        public DateTimeOffset Rise { get; set; }
        public long EpochRise { get; set; }
        public DateTimeOffset Set { get; set; }
        public long EpochSet { get; set; }
    }

    public class Headline
    {
        public DateTimeOffset EffectiveDate { get; set; }
        public long EffectiveEpochDate { get; set; }
        public long Severity { get; set; }
        public string Text { get; set; }
        public string Category { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public long EndEpochDate { get; set; }
        public Uri MobileLink { get; set; }
        public Uri Link { get; set; }
    }
}
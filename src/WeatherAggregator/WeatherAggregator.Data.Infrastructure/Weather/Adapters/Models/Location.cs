namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models
{
    public class Location
    {
        public string Code { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
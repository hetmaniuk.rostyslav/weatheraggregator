using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestEase;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Models;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.Mappers;
using WeatherAggregator.Data.Infrastructure.Weather.Adapters.REST;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters
{
    public class AccuWeatherProviderAdapter: IWeatherProvider
    {
        private readonly IAccuWeatherApiClient _apiClient;

        public AccuWeatherProviderAdapter()
        {
            _apiClient = RestClient.For<IAccuWeatherApiClient>(Constants.AccuWeatherServiceDetail.BasePath);
        }
        public async Task<ForecastResponseModel> GetDailyForecastAsync()
        {
            var result = await _apiClient.GetDailyForecast(
                Constants.DefaultLocation.Code,
                Constants.AccuWeatherServiceDetail.ApiKey);
            return result.ToResponseModel().First();
        }

        public async Task<IEnumerable<ForecastResponseModel>> GetWeeklyForecastsAsync()
        {
            var result = await _apiClient.GetWeaklyForecast(
                Constants.DefaultLocation.Code,
                Constants.AccuWeatherServiceDetail.ApiKey);
            return result.ToResponseModel();
        }
    }
}
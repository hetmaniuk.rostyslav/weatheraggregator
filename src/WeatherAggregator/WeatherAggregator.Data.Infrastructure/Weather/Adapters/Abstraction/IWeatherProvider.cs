using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherAggregator.Data.Infrastructure.Weather.ResponseModels;

namespace WeatherAggregator.Data.Infrastructure.Weather.Adapters.Abstraction
{
    public interface IWeatherProvider
    {
        Task<ForecastResponseModel> GetDailyForecastAsync();
        Task<IEnumerable<ForecastResponseModel>> GetWeeklyForecastsAsync();
    }
}
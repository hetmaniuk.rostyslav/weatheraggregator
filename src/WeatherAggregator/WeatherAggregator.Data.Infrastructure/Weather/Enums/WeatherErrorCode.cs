namespace WeatherAggregator.Data.Infrastructure.Weather.Enums
{
    public enum WeatherErrorCode
    {
        Unknown,
        InvalidProvider
    }
}
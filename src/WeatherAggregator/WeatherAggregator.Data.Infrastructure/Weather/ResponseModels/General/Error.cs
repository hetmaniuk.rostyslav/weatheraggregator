using System;
using System.Net;
using WeatherAggregator.Data.Infrastructure.Weather.Enums;
using WeatherAggregator.Data.Infrastructure.Weather.Exceptions;

namespace WeatherAggregator.Data.Infrastructure.Weather.ResponseModels.General
{
    public class Error
    {
        private const string DefaultErrorMessage = "Unknown Error";

        public WeatherErrorCode ErrorCode { get; }
        public HttpStatusCode StatusCode { get; }
        public string Message { get; }

        public Error(WeatherErrorCode errorCode, HttpStatusCode statusCode, string message = DefaultErrorMessage)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
            Message = message;
        }

        public Error(WeatherDomainException exception) : this(exception.ErrorCode, exception.StatusCode, exception.Message)
        {
        }
        
        public Error(WeatherErrorCode errorCode, HttpStatusCode statusCode, Exception exception) : this(errorCode, statusCode, exception.Message)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }
    }
}
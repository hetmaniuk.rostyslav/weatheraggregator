namespace WeatherAggregator.Data.Infrastructure.Weather.ResponseModels.General
{
    public class Response
    {
        public object Data { get; }
        public Error Error { get; }

        public Response(object data)
        {
            Data = data;
            Error = null;
        }
        
        public Response(Error error)
        {
            Data = null;
            Error = error;
        }
    }
}
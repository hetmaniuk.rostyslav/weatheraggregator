using System;

namespace WeatherAggregator.Data.Infrastructure.Weather.ResponseModels
{
    public class ForecastResponseModel
    {
        public DateTimeOffset Date { get; set; }
        public double MinTemp { get; set; }
        public double MaxTemp { get; set; }
        public double Temp { get; set; }
                              
        public double WindDirection { get; set; }
        
        public double WindSpeed { get; set; }
        
    }
}
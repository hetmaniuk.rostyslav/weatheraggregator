#!/usr/bin/env bash
dotnet restore ./src/WeatherAggregator/WeatherAggregator.sln
dotnet publish -c Release ./src/WeatherAggregator/WeatherAggregator.Web/WeatherAggregator.Web.csproj
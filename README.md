# WeatherAggregator

- provide two weather services (AccuWeather, OpenWeatherMao)

## Endpoints

http://localhost:5000
- daily forecast (api/forecasts/daily)
- weekly forecast (api/forecasts/weekly)

## Diagram

![](https://gitlab.com/hetmaniuk.rostyslav/weatheraggregator/blob/master/diagram.png)